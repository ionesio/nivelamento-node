# Nivelamento-Node

Código usado para explorar e aprender sobre a tecnologia usada na disciplina de Projeto 1

## Requisitos

### MongoDB
```bash
sudo apt install mongodb
```

### NPM
```bash
sudo apt install npm
```

## Setup
```
mkdir <database path>
mongod --dbpath=<database_path> --port=3001 &
npm install
node index
```
